<?php
    class Admin_model extends CI_Model {

            function __construct()
            {
                // Call the Model constructor
                parent::__construct();
				$this->load->database();
            }

			function get($table_name,$condition){
				$this->db->select("*");
				$this->db->where($condition);
				$this->db->from($table_name);
				$que=$this->db->get();
				return $que->result();
			}
			
			function get_count($table_name,$condition,$limit, $start){
				$this->db->select("*");
				$this->db->where($condition);
				$this->db->limit($limit, $start);
				$this->db->from($table_name);
				$que=$this->db->get();
				return $que->result();
			}
			
			
			function delete($table_name,$data){
				$this->db->where($data);
				$this->db->delete($table_name);
			}
			
			function insert($table_name,$data){	
				$this->db->insert($table_name,$data);
			}
			
			function update($table_name,$data,$condition){
				$this->db->where($condition);
				$this->db->update($table_name, $data);
			}
			
			function join_table($table_name1,$table_name2,$table_name3,$condition){
				$this->db->select('*');
				$this->db->from($table_name1);
				$this->db->join($table_name2,$table_name1.'.user_id='.$table_name2.'.user_id','Left');
				$this->db->join($table_name3,$table_name1.'.script_id='.$table_name3.'.script_id','Left');
				$this->db->where($condition);
				$query=$this->db->get();
				return $query->result();
			}
			
				function join_table1($table_name1,$table_name2,$table_name3,$condition,$limit, $start){
				$this->db->select('*');
				$this->db->from($table_name1);
				$this->db->join($table_name2,$table_name1.'.user_id='.$table_name2.'.user_id','Left');
				$this->db->join($table_name3,$table_name1.'.script_id='.$table_name3.'.script_id','Left');
				$this->db->limit($limit, $start);
				$this->db->where($condition);
				$query=$this->db->get();
				return $query->result();
			}
			
	
	
			
	}
	?>