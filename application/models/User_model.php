<?php
    class User_model extends CI_Model {

            function __construct()
            {
                // Call the Model constructor
                parent::__construct();
				$this->load->database();
            }
			
			function select($table_name,$condition){
				$this->db->select("*");
				$this->db->where($condition);
				$this->db->from($table_name);
				$que=$this->db->get();
				return $que->result();
			} 
			
			function update($table_name,$data,$condition){
				$this->db->where($condition);
				$this->db->update($table_name, $data);
				
			}
	}
?>