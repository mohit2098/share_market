<?php 
include 'include/header.php';

?>
<body>  
<?php
include 'include/admin_dash.php';
?>

		<div class="container">
			<div class="row">
			<div class="col-md-12">
			<form   method="post" action="<?php echo base_url();?>index.php/Admin_dashboard/buy_sell" enctype="multipart/form-data">			
				<div class="form-row ">
				
					<div class="form-group col-md-5">
						<label id="f1" for="first">Script Name</label>
						<select name="script_name" class="chosen-select form-control" data-placeholder="Chosen Example">
						<option disabled selected>Choose...</option>
						<?php
						 foreach ($info as $row)
						 {?>
							 <option value="<?php echo $row->script_id ;?>"><?php echo $row->script_name ;?></option>
						<?php }
						?>
						</select>
					</div>
					
					<div class="form-group col-md-3">
						<label id="f1" for="first">User Name</label>
						<select name="user_name" class="chosen-select form-control"  data-placeholder="Chosen Example">
						<option disabled selected>Choose...</option>
						<?php
						 foreach ($info1 as $row)
						 {?>
							 <option value="<?php echo $row->user_id ;?>"><?php echo $row->user_name ;?></option>
						<?php }
						?>
						</select>
					</div>
					
				</div>
				
				<div class="form-row ">
				
					<div class="form-group col-md-3">
						<label id="f1" for="first">price</label>
						<input class="form-control" name="price" id="price" placeholder="Price" type="text" required> 
					</div>
					
					<div class="form-group col-md-2">
						<label id="f1" for="first">Quantity</label>
						<input class="form-control" name="quantity" id="qty" placeholder="Quantity" type="text" required> 
					</div>
					<div class="form-group col-md-2">
						<div class="radio"><label>Buy</label>
						<input  name="buy_sell" value="Buy" type="radio">
						</div>
						</div>
					<div class="form-group col-md-2">
						<div class="radio"><label>Sell</label>
						<input name="buy_sell" value="Sell" type="radio">
						</div>
					</div>
				</div>
				
				<div class="form-row ">
				
				<div class="form-group col-md-3">
						<label id="f1" for="first">Total</label>
						<input class="form-control" name="total" readonly type="text" id="total">
					</div>
				</div>
				<div class="form-row ">
						<div class="form-row">
							<div class="form-group col-md-4">
							<br>
								<button class="btn btn-outline-danger" type="submit" id="button" name="add_script_btn" >Add</button>
							</div>
						</div>
				</div>
			</form>
				</div>
						</div>
				</div>
<?php
include 'include/footer.php';
?>
<script>
$('#price').on('keyup',function(){
    var tot = $('#qty').val() * this.value;
    $('#total').val(tot);
});
$('#qty').on('keyup',function(){
    var tot = $('#price').val() * this.value;
    $('#total').val(tot);
});
</script>
</body>

</html>