<div class="page-wrapper default-theme sidebar-bg bg1 toggled">
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <!-- sidebar-brand  -->
               <!-- <div class="sidebar-item sidebar-brand">
                    <a href="#">pro sidebar</a>
                </div> -->
                <!-- sidebar-header  -->
                <div class="sidebar-item sidebar-header d-flex flex-nowrap">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="img/user.jpg" alt="User picture">
                    </div>
                    <div class="user-info">
                        <span class="user-name"><?php echo $this->session->userdata('session_name');?>
                        
                        </span>
                        <span class="user-role">User</span>
                        <span class="user-status">
                            <i class="fa fa-circle"></i>
                            <span>Online</span>
                        </span>
                    </div>
                </div>
               
                <!-- sidebar-menu  -->
                <div class=" sidebar-item sidebar-menu">
                    <ul>
                        <li class="header-menu">
                            <span>General</span>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="<?php echo base_url()?>./index.php/user_dashboard/profile/<?php echo $this->session->userdata('session_id');?>">
                                <i class="fa fa-tachometer-alt"></i>
                                <span class="menu-text">View Profile</span>
                            </a>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="<?php echo base_url()?>./index.php/user_dashboard/edit/<?php echo $this->session->userdata('session_id');?>"">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="menu-text">Edit Profile</span>  
                            </a>    
                        </li>
						</ul>
					<ul>
                        <li class="header-menu">
                            <span>Other</span>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="<?php echo base_url()?>./index.php/user_dashboard/logout">
                                <i class="fa fa-tachometer-alt"></i>
                                <span class="menu-text" >Log Out</span>
                            </a>
                        </li>
					</ul>
                </div>
            </div>
        </nav>
    </div>