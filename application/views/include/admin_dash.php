    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
	<a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none">Start Bootstrap</span>
      
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
   <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link " href="<?php echo base_url()?>./Admin_dashboard/insert">Insert User</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url()?>./Admin_dashboard/show">View User</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()?>./Admin_dashboard/add_script">Add Script</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()?>./Admin_dashboard/show_script">View Script</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()?>./Admin_dashboard/buy_sell">Buy/Sell</a>
          </li>
          <li class="nav-item dropdown dropright">
            <a class="nav-link js-scroll-trigger dropdown-toggle "  id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Show Buy/Sell</a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" >
			<a class="dropdown-item" href="<?php echo base_url()?>./Admin_dashboard/show_buy">Buy</a>
			<div class="dropdown-divider"></div>
			<a class="dropdown-item" href="<?php echo base_url()?>./Admin_dashboard/show_sell">Sell</a>
		  </div>
		  
		  </li>
		  <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()?>./Admin_dashboard/report">Report</a>
          </li>
		  
		  <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url()?>./Admin_dashboard/logout">Log Out</a>
          </li>
		  
        </ul>
      </div>
	  </nav>






