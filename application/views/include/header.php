<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>System</title>
   
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-confirm.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/all.css">
    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css"> 
	<link rel="stylesheet" href="<?php echo base_url();?>assets/src/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/prism.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" >
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/devicons/css/devicons.min.css" >
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/resume.min.css" >
	
	<style>
	#myform .error{
		color:red;
	}
	#datepicker{width:180px; margin: 0 20px 20px 20px;}
#datepicker > span:hover{cursor: pointer;}
	
	</style>
</head>