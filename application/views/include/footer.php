

    <script src="<?php echo base_url();?>assets/src/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"> </script>
    <script src="<?php echo base_url();?>assets/js/jquery-confirm.min.js"> </script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"> </script>
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"> </script>
    <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/js/chosen.jquery.js"> </script>
    <script src="<?php echo base_url();?>assets/js/prism.js"> </script>
    <script src="<?php echo base_url();?>assets/js/init.js"> </script>
    <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/resume.min.js"></script>
    <script src="<?php echo base_url();?>assets/src/js/main.js"></script>
	
	
	<script>
	$(function () {
  $("#datepicker").datepicker({ 
        autoclose: true, 
        todayHighlight: true
		
  }).datepicker();
});

$('.delete').confirm({
    title: 'Delete',
    content: 'Do You Want to Delete!',
    buttons: {
        confirm: function () {
            location.href = this.$target.attr('href');
        },
        cancel: function () {
            $.alert('Canceled!');
        }
    }
});


</script>