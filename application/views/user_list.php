<?php 
include 'include/header.php';
?>
<body>  
<?php
include 'include/admin_dash.php';
?>

<table class="table table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Edit</th>
						<th>Delete</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody> 
         <?php  
         foreach ($info as $row)  
         {  
            ?><tr>   
            <td><?php echo $row->user_name;?></td>  
            <td><?php echo $row->user_email;?></td> 
			<td><?php echo $row->user_phone;?></td> 			
            <td><a href="<?php echo base_url()?>./index.php/Admin_dashboard/edit/<?php echo $row->user_id;?>">Edit</a></td>  
            <td><a class="delete" href="<?php echo base_url()?>./index.php/Admin_dashboard/delete/<?php echo $row->user_id;?>">Delete</a></td>
			<td>
			<a <?php if($row->user_status!="Active"){ ?>href="<?php echo base_url();?>index.php/Admin_dashboard/deactive/<?php echo $row->user_id;?>" <?php  }; if($row->user_status=="Active"){ ?>style="color:green" <?php  };  ?>>Active </a> |
			<a  <?php if($row->user_status!="Deactive"){ ?>href="<?php echo base_url();?>index.php/Admin_dashboard/active/<?php echo $row->user_id;?>" <?php  }; if($row->user_status=="Deactive"){ ?>style="color:red" <?php  };  ?>> Deactive</a>
			</td>
            </tr>  
         <?php }  
         ?>  
      </tbody>  
   </table>   
  <center> <p><?php if(isset($links)){echo($links);} ?></p></center>
<?php
include 'include/footer.php';
?>
<script>

</script>
</body>

</html>