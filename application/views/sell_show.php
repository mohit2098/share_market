<?php 
include 'include/header.php';
?>
<body>  
<?php
include 'include/admin_dash.php';
?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Script Name</th>
							<th>User Name</th>
							<th>Date</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody> 
					 <?php  
					 foreach ($info as $row)  
					 {  
						?><tr>   
						<td><?php echo $row->script_name;?></td>  
						<td><?php echo $row->user_name;?></td> 
						<td><?php echo $row->sell_date;?></td> 			
						<td><?php echo $row->sell_price;?></td>  
						<td><?php echo $row->sell_qty;?></td>
						<td><?php echo $row->sell_total;?></td>
						<td><a href="<?php echo base_url();?>index.php/Admin_dashboard/sell_edit/<?php echo $row->sell_id; ?>">Edit</a></td>
						</tr>  
					 <?php }  
					 ?>  
				  </tbody>  
				</table> 
				<center> <p><?php if(isset($links)){echo($links);} ?></p></center>
		
	
<?php
include 'include/footer.php';
?>
</body>

</html>