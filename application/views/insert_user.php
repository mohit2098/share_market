<?php 
include 'include/header.php';
?>
<body>  

<?php
include 'include/admin_dash.php';
?>
<form class="jumbotron" style="height:95%;" method="post" id="myform" action="<?php echo base_url();?>index.php/Admin_dashboard/insert" enctype="multipart/form-data">
				
				<div class="form-row ">
					<div class="form-group col-md-3">
						<label id="f1" for="first">Name</label>
						<input class="form-control" id="first" type="text" name="name" placeholder="Name">
					</div>
					
					<div class="form-group col-md-3">
							<label for="pass1">Password</label>
							<input class="form-control" id="pass" type="password" name="pass" placeholder="Password">
					</div>
					
					<div class="form-group col-md-3">
						<label id="f1" for="first">Email</label>
						<input class="form-control" id="email" type="text" name="email" placeholder="Email">
					</div>
				</div>
				
				
				
				<div class="form-row ">
					
					<div class="form-group col-md-2">
							<label for="pass1">Account Type</label>
							<select class="form-control" id="account_type" name="account_type">
								<option disabled selected>Choose...</option>
								<option value="Equity">Equity</option>
								<option value="Commodity">Commodity</option>
								<option value="Both">Both</option>
							</select>
					</div>
					
					<div class="form-group col-md-2">
							<label for="pass1">User Type</label>
							<select class="form-control" id="user_type" name="user_type">
								<option disabled selected>Choose...</option>
								<option value="Admin">Admin</option>
								<option value="Normal">Normal</option>
							</select>
					</div>
					
					<div class="form-group col-md-2">
						<label id="f1" for="first">Status</label>
						<select class="form-control" id="status" name="status">
								<option disabled selected>Choose...</option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
		
					</div>
					
					<div class="form-group col-md-3">
						<label id="f1" for="first">Address</label> 
						<input class="form-control" id="address" type="text" name="address" placeholder="Address">
					</div>
				</div>
				
				
				
				<div class="form-row ">
				
					<div class="form-group col-md-3">
						<label id="f1" for="firstq">Date of Birth</label>
						<div id="datepicker" class="input-group date " data-date-format="yyyy-mm-dd" data-date-end-date="-1d">
							<input class="form-control" id="firstq" type="text" name="dob"/>
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					
					
					
					<div class="form-group col-md-3">
						<label  for="first">Adahar Card</label>
						<input class="form-control" type="text" name="adahar_card" placeholder="Adahar Card">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Pan Card</label>
						<input class="form-control" type="text" name="pan_card" placeholder="Pan Card">
					</div>
					
			
				</div>
				
				
				
				<div class="form-row ">
				
					<div class="form-group col-md-3">
						<label  for="first">Bank Account Number</label>
						<input class="form-control" type="text" name="account_number" placeholder="Bank Account Number">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">IFSC Code</label>
						<input class="form-control" type="text" name="ifsc_code" placeholder="IFSC Code">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Bank Name</label>
						<input class="form-control" type="text" name="bank_name" placeholder="Bank Name">
					</div>
					
					
				</div>
				
				<div class="form-row">
				<div class="form-group col-md-3">
						<label  for="first">Limit</label>
						<input class="form-control" type="text" name="limit" placeholder="Limit">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Deposit Amount</label>
						<input class="form-control" type="text" name="deposit_amount" placeholder="Deposit Amount">
					</div>
					
					<div class="form-group col-md-3">
							<label for="pass1">Phone</label>
							<input class="form-control" id="phone" type="text" name="phone" placeholder="Phone">
						
					</div>
				</div>
				
				
				<div class="form-row">
						<div class="form-group col-md-4">
						<br>
							<button class="btn btn-outline-warning" type="submit" id="button" name="fac_log_btn">Insert</button>
						</div>
					</div>
				
				
			</form>


<?php
include 'include/footer.php';
?>
<script>
$("#myform").validate({
		rules: {
			email: {
				required: true,
				email: true,
				remote: {
					url:"<?php echo base_url(); ?>index.php/Admin_dashboard/Checkemail",
					type: "post"
				 }
			},
			
			phone: {
				required: true,
				remote: {
					url:"<?php echo base_url(); ?>index.php/Admin_dashboard/Checkphone",
					type: "post"
				 }
			}
		},
		messages: {
			email:{
				required: "**Please provide a Email",
				remote: "***Email already in use!",
			},
			phone:{
				required: "**Please provide a phone",
				remote: "***phone already in use!",
			},
		},
		submitHandler: function(form) {
			form.submit();
		}
	});
	



</script>
</body>

</html>