<?php 
include 'include/header.php';
?>
<body>  

<?php
include 'include/admin_dash.php';
?>
<?php  	 
$segment=$this->uri->segment(3);
?>
<table class="table table-hover">
				<thead>
					<tr>
						<th>S.NO.</th>
						<th>Name</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody> 
         <?php 
		$count=1;
		if($segment)
			 $count=$segment+1;		 
         foreach ($info as $row)  
         {  
            ?><tr>   
            <td><?php echo $count; ?></td>  
            <td><?php echo $row->script_name;?></td>  			
            <td><a href="<?php echo base_url()?>./index.php/Admin_dashboard/edit_script/<?php echo $row->script_id;?>">Edit</a></td>  
            <td><a class="delete" href="<?php echo base_url()?>./index.php/Admin_dashboard/delete_script/<?php echo $row->script_id;?>">Delete</a></td>
            </tr>  
         <?php 
		 $count=$count+1;
		 }  
         ?>  
      </tbody>  
   </table>   
   <center> <p><?php if(isset($links)){echo($links);} ?></p></center>
  
<?php
include 'include/footer.php';
?>
</body>

</html>