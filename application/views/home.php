<html>
  <head>
    <title>System Login</title>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/css/design.css" rel="stylesheet" type="text/css">
	<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </head>
  <body>
	  <div class="container">
		<div class="row">
		  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
			  <div class="card-body">
				<h3 class="card-title text-center">Sign In</h3>
				<form class="form-signin" action="<?php echo base_url();?>index.php/home/check" method="post">
				  <div class="form-label-group">
					<input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus name="user_email">
					<label for="inputEmail">Email address</label>
				  </div>
				  <div class="form-label-group">
					<input type="password" id="inputPassword" class="form-control" name="pass" placeholder="Password" required>
					<label for="inputPassword">Password</label>
				  </div>
				  <hr class="my-4">
				  <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="log_btn">Sign in</button>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</body>
</html>