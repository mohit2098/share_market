<?php 
include 'include/header.php';
?>
<body>  
<?php
include 'include/admin_dash.php';
?>
<form class="jumbotron"  method="post" action="<?php echo base_url();?>index.php/Admin_dashboard/report_show">
	<div class="form-row">
		<div class="form-group col-md-3">
			<label for="user_name">User Name</label>
			<input type="text" class="form-control" id="user_name" placeholder="Enter User Name">
		</div>
		<div class="form-group col-md-1">
		<label for="today"></label><br>
			<button class="btn btn-primary" id="today">Today</button>
		</div>
		<div class="form-group col-md-1">
		<label for="all"></label><br>
			<button class="btn btn-primary" id="all">All</button>
		</div>
		
		<div class="form-group col-md-3">
			<label for="start_date">Start Date</label>
			<input type="text" class="form-control" id="start_date" placeholder="Enter Start Date">
		</div>
		<div class="form-group col-md-3">
			<label for="end_date">End Date</label>
			<input type="text" class="form-control" id="end_date" placeholder="Enter End Date">
		</div>
	</div>
	<div class="form-row">
		<div class="form-group col-md-4"><br>
			<button class="btn btn-outline-danger" type="submit" id="button" name="filter" >Filter</button>
		</div>
	</div>
<div class="dropdown-divider"></div>
</form>
<?php

?>
<?php
include 'include/footer.php';
?>
</body>

</html>