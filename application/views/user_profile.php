<?php 
include 'include/header.php';
?>
<body>  
<div class="row">
<div class="col-md-3">
<?php
include 'include/user_dash.php';

?>
</div>
<div class="col-md-9">

<table class="table table-hover">
				
				
				<tbody>
					<tr>
						<th>Name : </th>
						<td  align="justify"><?php echo $info[0]->user_name; ?></td>
						<th>Email : </th>
						<td><?php echo $info[0]->user_email; ?></td>
					</tr>
					
					<tr>
						<th>Phone :</th>
						<td><?php echo $info[0]->user_phone; ?></td>
						<th>Address : </th>
						<td  align="justify"><?php echo $info[0]->user_address; ?></td>
					</tr>
					
						<tr>
						<th>Type : </th>
						<td  align="justify"><?php echo $info[0]->user_type; ?></td>
						<th>Status : </th>
						<td><?php echo $info[0]->user_status; ?></td>
					</tr>
					
					<tr>
						<th>Create At :</th>
						<td><?php echo $info[0]->create_at; ?></td>
						<th>Update At : </th>
						<td  align="justify"><?php echo $info[0]->update_at; ?></td>
					</tr>
					
					<tr>
						<th>DOB : </th>
						<td  align="justify"><?php echo $info[0]->user_dob; ?></td>
						<th>Adahar number : </th>
						<td><?php echo $info[0]->User_adahar_card; ?></td>
					</tr>
					
					<tr>
						<th>PAN Number :</th>
						<td><?php echo $info[0]->user_pan_card; ?></td>
						<th>Account Number : </th>
						<td  align="justify"><?php echo $info[0]->user_account_number; ?></td>
					</tr>
					
					<tr>
						<th>IFSC Code :</th>
						<td><?php echo $info[0]->user_ifsc; ?></td>
						<th>Bank Name : </th>
						<td  align="justify"><?php echo $info[0]-> user_bank_name ; ?></td>
					</tr>
					
					<tr>
						<th>Limit :</th>
						<td><?php echo $info[0]-> user_limit ; ?></td>
						<th>Deposit Amount : </th>
						<td  align="justify"><?php echo $info[0]->user_deposit; ?></td>
					</tr>
					<tr>
						
					<th>Account Type : </th>
						<td  align="justify"><?php echo $info[0]->user_account_type; ?></td>
					</tr>
					
				</tbody>


</table>

</div>
</div>
<?php
include 'include/footer.php';
?>
</body>

</html>