<?php 
include 'include/header.php';
?>
<body>  
<?php
include 'include/admin_dash.php';
?>

<form class="jumbotron"  method="post" action="<?php echo base_url();?>index.php/Admin_dashboard/edit_info" enctype="multipart/form-data">
				
				<div class="form-row ">
					<div class="form-group col-md-3">
						<label id="f1" for="first">Name</label>
						<input class="form-control" id="first" type="text" name="name" value="<?php echo $info[0]->user_name; ?>" >
						<input type="hidden" value="<?php echo $info[0]->user_id; ?>" name="id">
					</div>
					
					<div class="form-group col-md-3">
							<label for="pass1">Password</label>
							<input class="form-control" id="pass" type="password" name="pass" placeholder="Password">
					</div>
					
					<div class="form-group col-md-3">
						<label id="f1" for="first">Email</label>
						<input class="form-control" id="email" type="text" name="email" readonly value="<?php echo $info[0]->user_email; ?>">
					</div>
				</div>
				
				
				
				<div class="form-row ">
					
					<div class="form-group col-md-2">
							<label for="pass1">Account Type</label>
							<select class="form-control" id="account_type" name="account_type">
								<option disabled selected>Choose...</option>
								<option value="Equity" <?php if($info[0]->user_account_type=="Equity"){?> selected <?php }?> >Equity</option>
								<option value="Commodity" <?php if($info[0]->user_account_type=="Commodity"){?> selected <?php }?>>Commodity</option>
								<option value="Both" <?php if($info[0]->user_account_type=="Both"){?> selected <?php }?>>Both</option>
							</select>
					</div>
					
					<div class="form-group col-md-2">
							<label for="pass1">User Type</label>
							<select class="form-control" id="user_type" name="user_type">
							<option disabled selected>Choose...</option>
								<option value="Admin" <?php if($info[0]->user_type=="Admin"){?> selected <?php }?>>Admin</option>
								<option value="Normal" <?php if($info[0]->user_type=="Normal"){?> selected <?php }?>>Normal</option>
							</select>
					</div>
					
					<div class="form-group col-md-2">
						<label id="f1" for="first">Status</label>
						<select class="form-control" id="status" name="status">
								<option disabled selected>Choose...</option>
								<option value="Active" <?php if($info[0]->user_status=="Active"){?> selected <?php }?>>Active</option>
								<option value="Deactive" <?php if($info[0]->user_status=="Deactive"){?> selected <?php }?>>Deactive</option>
							</select>
		
					</div>
					
					<div class="form-group col-md-3">
						<label id="f1" for="first">Address</label> 
						<input class="form-control" id="address" type="text" name="address" value="<?php echo $info[0]->user_address; ?>">
					</div>
				</div>
				
				
				
				<div class="form-row ">
				
					
					<div class="form-group col-md-3">
						<label id="f1" for="firstq">Date of Birth</label>
						<div id="datepicker" class="input-group date " data-date-format="yyyy-mm-dd" data-date-end-date="-1d">
							<input class="form-control" id="firstq" type="text" name="dob" value="<?php echo $info[0]->user_dob; ?>"/>
							<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
						</div>
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Adahar Card</label>
						<input class="form-control" type="text" name="adahar_card" value="<?php echo $info[0]->User_adahar_card; ?>">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Pan Card</label>
						<input class="form-control" type="text" name="pan_card" value="<?php echo $info[0]->user_pan_card; ?>">
					</div>
					
			
				</div>
				
				
				
				<div class="form-row ">
				
					<div class="form-group col-md-3">
						<label  for="first">Bank Account Number</label>
						<input class="form-control" type="text" name="account_number" value="<?php echo $info[0]->user_account_number; ?>">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">IFSC Code</label>
						<input class="form-control" type="text" name="ifsc_code" value="<?php echo $info[0]->user_ifsc; ?>">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Bank Name</label>
						<input class="form-control" type="text" name="bank_name" value="<?php echo $info[0]->user_bank_name; ?>">
					</div>
					
					
				</div>
				
				<div class="form-row">
				<div class="form-group col-md-3">
						<label  for="first">Limit</label>
						<input class="form-control" type="text" name="limit" value="<?php echo $info[0]->user_limit; ?>">
					</div>
					
					<div class="form-group col-md-3">
						<label  for="first">Deposit Amount</label>
						<input class="form-control" type="text" name="deposit_amount" value="<?php echo $info[0]->user_deposit; ?>">
					</div>
					
					<div class="form-group col-md-3">
							<label for="pass1">Phone</label>
							<input class="form-control" id="phone" type="text" name="phone" value="<?php echo $info[0]->user_phone; ?>">
					</div>
				</div>
				
				
				<div class="form-row">
						<div class="form-group col-md-4">
						<br>
							<button class="btn btn-outline-danger" type="submit" id="button" name="fac_log_btn" >Update</button>
						</div>
					</div>
				
				
			</form>


<?php
include 'include/footer.php';
?>
</body>

</html>