<?php 
include 'include/header.php';
?>
<body>  
<?php
include 'include/admin_dash.php';
?>

			<ul class="nav nav-tabs"  role="tablist">
			 <li class="nav-item "><a  class="nav-link" role="tab" data-toggle="tab" href="#buy">Buy</a></li>
			  <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#sell">Sell</a></li>
			</ul>

		<div class="tab-content">
		    <div id="buy" class="tab-pane fade show  " role="tabpanel">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Script Name</th>
							<th>User Name</th>
							<th>Date</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody> 
					 <?php  
					 foreach ($info as $row)  
					 {  
						?><tr>   
						<td><?php echo $row->script_name;?></td>  
						<td><?php echo $row->user_name;?></td> 
						<td><?php echo $row->buy_date;?></td> 			
						<td><?php echo $row->buy_price;?></td>  
						<td><?php echo $row->buy_qty;?></td>
						<td><?php echo $row->buy_total;?></td>
						<td><a href="<?php echo base_url();?>index.php/Admin_dashboard/buy_edit/<?php echo $row->buy_id; ?>">Edit</a></td>
						</tr>  
					 <?php }  
					 ?>  
				  </tbody>  
				</table>   
			</div>
		    <div id="sell" class="tab-pane fade show active" role="tabpanel">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Script Name</th>
							<th>User Name</th>
							<th>Date</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody> 
					 <?php  
					 foreach ($info1 as $row)  
					 {  
						?><tr>   
						<td><?php echo $row->script_name;?></td>  
						<td><?php echo $row->user_name;?></td> 
						<td><?php echo $row->sell_date;?></td> 			
						<td><?php echo $row->sell_price;?></td>  
						<td><?php echo $row->sell_qty;?></td>
						<td><?php echo $row->sell_total;?></td>
						<td><a href="<?php echo base_url();?>index.php/Admin_dashboard/sell_edit/<?php echo $row->sell_id; ?>">Edit</a></td>
						</tr>  
					 <?php }  
					 ?>  
				  </tbody>  
				</table> 
			</div>
		</div>
			
	
<?php
include 'include/footer.php';
?>
</body>

</html>