<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_dashboard extends CI_Controller {
	
	public function __construct()
	{
	parent::__construct();
	$this->load->library(array("form_validation","session"));
	$this->load->helper(array('form','url','html','captcha'));
	$this->load->model('User_model');
	}

	public function index()
	{	
			if($this->session->userdata('session_id')){
			$this->load->view('../views/user_view');
		}else{
			redirect(base_url()."index.php/home"); 
			
		}
	}
	public function logout()
	{	
		
		$this->session->unset_userdata('session_name');
		$this->session->unset_userdata('session_id');
		redirect(base_url()."/index.php/home"); 
		//$this->load->view('../views/home');
	}
	
	public function profile($var)
	{
		
		$d=[
		'user_id'=>$var,
		];
		
		$data['info']=$this->User_model->select("user_table",$d);
		$this->load->view('../views/user_profile',$data);
	}
	
	public function edit($var)
	{
		
		$d=[
		'user_id'=>$var,
		];
		$data['info']=$this->User_model->select("user_table",$d);
		$this->load->view('../views/edit_profile',$data);
	}
	public function edit_info()
	{
			
			 $data = [
					'user_name' => $this->input->post('name'),
					'user_phone'=>$this->input->post('phone'),
					'user_address'=>$this->input->post('address'),
					'user_deposit'=>$this->input->post('deposit_amount'),
					'user_bank_name'=>$this->input->post('bank_name'),
					'user_ifsc'=>$this->input->post('ifsc_code'),
					'user_account_number'=>$this->input->post('account_number'),
					'user_pan_card'=>$this->input->post('pan_card'),
					'User_adahar_card'=>$this->input->post('adahar_card'),
					'update_at'=>date('Y-m-d H:i:s'),
				];
				
				$condition=[
				'user_id'=>$this->input->post('id'),
				];
			 
				$this->User_model->update("user_table",$data,$condition);
	
				$data['info']=$this->User_model->profile_info($this->input->post('id'));
				$this->load->view('../views/edit_profile',$data);
	}
}