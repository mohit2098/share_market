<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array("form_validation","session"));
		$this->load->helper(array('form','url','html','captcha'));
		$this->load->model('User');
	}

	public function index()
	{	
		
		
		$this->load->view('../views/home');
	}
	
	
	function check() {
		if($this->input->post('user_email')){
			 $data=array();
			 $email=$this->input->post('user_email');
			 $pass=$this->input->post('pass');
			 $data=$this->User->check($email,$pass);
			 $count=count($this->User->check($email,$pass));
			if($count!=0)
			 {
				 $this->session->set_userdata('session_id',$data[0]->user_id);
				 $this->session->set_userdata('session_name',$data[0]->user_name);
				 $this->session->set_userdata('session_type',$data[0]->user_type);
				 
				 if($data[0]->user_type=="Admin"){
					redirect(base_url()."index.php/Admin_dashboard"); 
				}
				else if($data[0]->user_type=="Normal")  
				 {
				   if($data[0]->user_status=="Deactive")
				   {
					   redirect(base_url()."index.php/home");	
				   }
				   else{
					   redirect(base_url()."index.php/User_dashboard");
				   }
				  	 
				 }
				}
			 else
			 {
				 redirect(base_url()."index.php/home");	 
			 }
		}
       }
	
	
}
