<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_dashboard extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array("form_validation","session","pagination"));
		$this->load->helper(array('form','url','html','captcha'));
		$this->load->model('Admin_model');
	}

	public function index()
	{	
		
		if($this->session->userdata('session_id')){
			$this->load->view('../views/admin_view');
		}
		else{
			redirect(base_url()."index.php/home"); 	
		}
		
	}
	public function logout()
	{	
		$this->session->unset_userdata('session_name');
		$this->session->unset_userdata('session_id');
		redirect(base_url()."/index.php/home"); 
	}
	public function show()
	{
		$condition=['user_type'=>'Normal',];
		$count1=count($this->Admin_model->get('user_table',$condition));
		$config = array();
        $config["base_url"] = base_url() . "Admin_dashboard/show";
        $config["total_rows"] = $count1;
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
		$data['info']=$this->Admin_model->get_count('user_table',$condition,$config["per_page"],$page);
		$this->load->view('../views/user_list',$data);
	}
	
	
	
	public function delete($var)
	{
		
		$data=[
		'user_id'=>$var,
		];
		$this->Admin_model->delete('user_table',$data);
		$condition=['user_type'=>'Normal',];
		$data['info']=$this->Admin_model->get('user_table',$condition);
		$this->load->view('../views/user_list',$data);		
	}
	
	
	
	public function edit($var)
	{
		
		$condition=[
		'user_id'=>$var,
		];
		$data['info']=$this->Admin_model->get('user_table',$condition);
		$this->load->view('../views/edit_user',$data);
	}
	
		
	
	
	public function edit_info()
	{		
			$data = [
					'user_name' => $this->input->post('name'),
					'user_phone'=>$this->input->post('phone'),
					'user_email'=>$this->input->post('email'),
					'user_type'=>$this->input->post('user_type'),
					'user_dob'=>$this->input->post('dob'),
					'user_address'=>$this->input->post('address'),
					'user_status'=>$this->input->post('status'),
					'user_account_type'=>$this->input->post('account_type'),
					'user_deposit'=>$this->input->post('deposit_amount'),
					'user_limit'=>$this->input->post('limit'),
					'user_bank_name'=>$this->input->post('bank_name'),
					'user_ifsc'=>$this->input->post('ifsc_code'),
					'user_account_number'=>$this->input->post('account_number'),
					'user_pan_card'=>$this->input->post('pan_card'),
					'User_adahar_card'=>$this->input->post('adahar_card'),
					'update_at'=>date('Y-m-d H:i:s'),
				];
				
				if(strlen( $this->input->post('pass'))!=0)
				$data=[
				'user_password'=>md5($this->input->post('pass')),
				];
				
				$condition=[
				'user_id'=>$this->input->post('id'),
				];
				
			 $this->Admin_model->update("user_table",$data,$condition);
			
			$condition=[
			'user_type'=>'Normal',
			];
			$data['info']=$this->Admin_model->get('user_table',$condition);
			$this->load->view('../views/user_list',$data);
			
			
	}
	
	
	public function insert()
	{
			if($this->input->post('name')){
				 $data = [
					'user_name' => $this->input->post('name'),
					'user_phone'=>$this->input->post('phone'),
					'user_password'=>md5($this->input->post('pass')),
					'user_email'=>$this->input->post('email'),
					'user_type'=>$this->input->post('user_type'),
					'user_dob'=>$this->input->post('dob'),
					'user_address'=>$this->input->post('address'),
					'user_status'=>$this->input->post('status'),
					'create_at'=>date('Y-m-d H:i:s'),
					'update_at'=>date('Y-m-d H:i:s'),
					'user_account_type'=>$this->input->post('account_type'),
					'user_deposit'=>$this->input->post('deposit_amount'),
					'user_limit'=>$this->input->post('limit'),
					'user_bank_name'=>$this->input->post('bank_name'),
					'user_ifsc'=>$this->input->post('ifsc_code'),
					'user_account_number'=>$this->input->post('account_number'),
					'user_pan_card'=>$this->input->post('pan_card'),
					'User_adahar_card'=>$this->input->post('adahar_card'),
				   ];
				$this->Admin_model->insert('user_table',$data);
			    redirect(base_url()."/index.php/Admin_dashboard/show");	
			 }
			 else{
				 $this->load->view('../views/insert_user');
			 }		
	}
	
	
	public function Checkemail(){
	$condition=[
			'user_email'=> $this->input->post('email'),
			];
	$count1=count($this->Admin_model->get('user_table',$condition));
		if($count1 == 0)
			echo 'true';
		else
			echo 'false';
	}
	
	
	public function Checkphone(){
	$condition=[
			'user_phone'=> $this->input->post('phone'),
			];
	$count2=count($this->Admin_model->get('user_table',$condition));
		if($count2 == 0)
		echo 'true';
		else
			echo 'false';
	}
	
	
	public function active($var){
		
		$data=[
				'user_status'=>'Deactive',
				];
		$condition=[
				'user_id'=>$var,
				];
		$this->Admin_model->update('user_table',$data,$condition);
		redirect(base_url()."/index.php/Admin_dashboard/show");
		
	}
	
	public function deactive($var){
		$data=[
				'user_status'=>'Active',
				];
		$condition=[
				'user_id'=>$var,
				];
		$this->Admin_model->update('user_table',$data,$condition);
		redirect(base_url()."/index.php/Admin_dashboard/show");	
	}
	
	public function edit_script($var)
	{
		$condition=[
		'script_id'=>$var,
		];
		$data['info']=$this->Admin_model->get('script_table',$condition);
		$this->load->view('../views/edit_script',$data);
	}
	
	public function edit_script_info()
	{
		$condition=[
		'script_id'=>$this->input->post('script_id'),
		];
		$data=[
		'script_name'=>$this->input->post('script_name'),
		];
		$this->Admin_model->update('script_table',$data,$condition);
		redirect(base_url()."/index.php/Admin_dashboard/show_script"); 
	}
	
	
	public function show_script()
	{
		$condition=[];
		
		$count1=count($this->Admin_model->get('script_table',$condition));
		
		$config = array();
        $config["base_url"] = base_url() . "Admin_dashboard/show_script";
        $config["total_rows"] = $count1;
        $config["per_page"] = 2;
        $config["uri_segment"] = 3;
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link" tabindex="1">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link" tabindex="1">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link" tabindex="1">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link"tabindex="1">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link" >';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
		$data['info']=$this->Admin_model->get_count('script_table',$condition,$config["per_page"],$page);
		$this->load->view('../views/script_list',$data);
	}
	
	public function add_script()
	{
		if($this->input->post('script_name')){
			$data=[
			'script_name'=>$this->input->post('script_name'),
			];
			$this->Admin_model->insert('script_table',$data);
			redirect(base_url()."/index.php/Admin_dashboard/show_script"); 
		}
		else{
		$this->load->view('../views/add_script');
		}
		
	}
	
	public function delete_script($var)
	{
		$data=[
		'script_id'=>$var,
		];
		$this->Admin_model->delete('script_table',$data);
		redirect(base_url()."/index.php/Admin_dashboard/show_script"); 		
	}
	
	public function buy_sell()
	{
		if($this->input->post('price')){
			if(($this->input->post('buy_sell'))=="Buy"){
				$data=[
				 'user_id'=>$this->input->post('user_name'),
				 'script_id'=>$this->input->post('script_name'),
				 'buy_price'=>$this->input->post('price'),
				 'buy_qty'=>$this->input->post('quantity'),
				 'buy_total'=>$this->input->post('total'),
				 'buy_date'=>date('Y-m-d'),
				];
				$this->Admin_model->insert('buy_table',$data);
			}
			else{
				$data=[
				 'user_id'=>$this->input->post('user_name'),
				 'script_id'=>$this->input->post('script_name'),
				 'sell_price'=>$this->input->post('price'),
				 'sell_qty'=>$this->input->post('quantity'),
				 'sell_total'=>$this->input->post('total'),
				 'sell_date'=>date('Y-m-d'),
				];
				$this->Admin_model->insert('sell_table',$data);	
			}	
		}
		$con=[];
		$data['info']=$this->Admin_model->get('script_table',$con);
		$data['info1']=$this->Admin_model->get('user_table',$con);
		$this->load->view('../views/buy_sell_form',$data);	
	}
	
	public function show_buy(){
		$con=[];
		$count1=count($this->Admin_model->get('buy_table',$con));
		$config = array();
        $config["base_url"] = base_url() . "Admin_dashboard/show_buy";
        $config["total_rows"] = $count1;
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
		$data['info']=$this->Admin_model->join_table1('buy_table','user_table','script_table',$con,$config["per_page"],$page);
		$this->load->view('../views/buy_show',$data);		
	}
	
	public function show_sell(){
		$con=[];
		$count1=count($this->Admin_model->get('sell_table',$con));
		$config = array();
        $config["base_url"] = base_url() . "Admin_dashboard/show_sell";
        $config["total_rows"] = $count1;
        $config["per_page"] = 1;
        $config["uri_segment"] = 3;
		$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
		$config['full_tag_close'] 	= '</ul></nav></div>';
		$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['num_tag_close'] 	= '</span></li>';
		$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close'] 	= '</span></li>';
		$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close'] 	= '</span></li>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["links"] = $this->pagination->create_links();
		$data['info']=$this->Admin_model->join_table1('sell_table','user_table','script_table',$con,$config["per_page"],$page);
		$this->load->view('../views/sell_show',$data);		
	}
	
	public function buy_edit($var){
		$con=[
				'buy_id'=>$var,
				];
		$con1=[];
		$data['info']=$this->Admin_model->join_table('buy_table','user_table','script_table',$con);
		$data['info1']=$this->Admin_model->get('script_table',$con1);
		$data['info2']=$this->Admin_model->get('user_table',$con1);
		$this->load->view('../views/buy_edit',$data);	
	}
	
	public function edit_buy(){
		$data=[
		'user_id'=>$this->input->post('user_name'),
		'script_id'=>$this->input->post('script_name'),
		'buy_price'=>$this->input->post('price'),
		'buy_qty'=>$this->input->post('quantity'),
		'buy_total'=>$this->input->post('total'),
		
		];
		$con=[
				'buy_id'=>$this->input->post('buy_id'),
				];
				
		$this->Admin_model->update('buy_table',$data,$con);
		redirect(base_url()."/index.php/Admin_dashboard/show_buy"); 
	}
	
	public function sell_edit($var){
		$con=[
				'sell_id'=>$var,
				];
				
		$con1=[];
		$data['info']=$this->Admin_model->join_table('sell_table','user_table','script_table',$con);
		$data['info1']=$this->Admin_model->get('script_table',$con1);
		$data['info2']=$this->Admin_model->get('user_table',$con1);
		$this->load->view('../views/sell_edit',$data);	
		
	}
	
	public function edit_sell(){
		$data=[
		'user_id'=>$this->input->post('user_name'),
		'script_id'=>$this->input->post('script_name'),
		'sell_price'=>$this->input->post('price'),
		'sell_qty'=>$this->input->post('quantity'),
		'sell_total'=>$this->input->post('total'),
		];
		$con=[
				'sell_id'=>$this->input->post('sell_id'),
				];
				
		$this->Admin_model->update('sell_table',$data,$con);
		redirect(base_url()."/index.php/Admin_dashboard/show_sell"); 
	}
	
	public function report(){
		$filter=[];
		
		/*if(!empty($this->session->userdata('user_name'))){
			$filter['user_name']=$this->session->userdata('user_name');
		}
		if(!empty($this->session->userdata('start_date'))){
			$filter['start_date']=$this->session->userdata('start_date');
		}
		if(!empty($this->session->userdata('end_date'))){
			$filter['end_date']=$this->session->userdata('end_date');
		}*/
		
		$data['info']=$this->Admin_model->join_table('buy_table','user_table','script_table',$filter);
		$data['info1']=$this->Admin_model->join_table('sell_table','user_table','script_table',$filter);
		$this->load->view('../views/report',$data);	
	}
	
	public function report_show(){
		if($this->input->post('user_name')){
			$filter=$this->input->post('user_name');
			 $this->session->set_userdata('filter',$filter);
		}
		if($this->input->post('start_date')){
			$filter_start=$this->input->post('start_date');
			$this->session->set_userdata('filter_start',$filter_start);
		}
		if($this->input->post('end_date')){
			$filter_end=$this->input->post('end_date');
			$this->session->set_userdata('filter_end',$filter_end);
		}
		redirect(base_url()."/index.php/Admin_dashboard/report");
	}
	
	
	


	
	
}